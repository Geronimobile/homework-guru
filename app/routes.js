// app/routes.js
module.exports = function(app, passport) {

	require('../scripts/profile.js')(app, passport);

	var paypal = require('paypal-rest-sdk');	
	var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'malott.jonathan@gmail.com',
                pass: 'hook9435bench'
            }
        });

        var mandrill = require('node-mandrill')('tDv0RopFBHZb_Br26if3ig');
	
	paypal.configure({
	  'mode': 'sandbox', //sandbox or live
	  'client_id': 'Ad7vv92aF9xi6rc4rT0uofXBHKc-Io5-6qrDy9U6V6N-AqP2gjDd9LNn_jEgjOt_qjH2anzexm09Zb0f',
	  'client_secret': 'EFqeYCrKB-BdyV2AMFjGSCv1GF0KPtLAqclJcW98Bqo3R0YDtga5kSp5_Fz4PkkfSkI-txNNCscBgbeE'
	});

	app.get('/', function(req, res) {
		res.render('index.ejs'); // load the index.ejs file
	});

	app.get('/termsofuse', function(req, res) {
		res.render('termsofuse.ejs'); // load the index.ejs file
	});

	app.get('/howitworks', function(req, res) {
		res.render('howitworks.ejs'); // load the index.ejs file
	});

	app.get('/faq', function(req, res) {
		res.render('faq.ejs'); // load the index.ejs file
	});

	app.get('/submit_refund',isLoggedIn, function(req, res) {

		if(req.query.id == undefined){
			res.redirect('/profile');
			console.log("id is undefined");
			return;
		}

		refund(req.query.email,req.query.id,"not found");

		res.redirect('/profile');

	});

	function refund (email,id,reason,q){

        console.log("issuing notice to "+id);

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);
	
	    connection.query("update `" + dbconfig.database + "`.question set refunded = 1 where paymentID='"+id+"'", function(err, rows, fields) {
			console.log("Database updated: "+id);

			var messg = "";

			if(reason == "timeout"){
				messg = "We could not find your question within the 24 hour window that we promised. We will not charge you any money on paypal for this question. We hope that you ask another question on Homework.Guru in the future!";
			} else if (reason == "not found"){
				messg = "We looked all over the internet but could not find a sufficient answer to your question. We will not charge you any money on paypal for this question. Please ask more questions in the futue on Homework.Guru!";
			} else {		
				messg = "We looked all over the internet but could not find a sufficient answer to your question. We will not charge you any money on paypal for this question. Please ask more questions in the futue on Homework.Guru!";
			}

		        mandrill('/messages/send', {
		            message: {
		                to: [{email: email}],
		                from_email: 'no-reply@homework.guru',
		                from_name: 'Homework Guru',
		                subject: "Homework.Guru Couldn't Find an Answer for You",
		                text: messg+"\n\nFor Reference, you submitted this question:\n\n"+q
		            }
		        }, function(error, response){
		            if (error) console.log( JSON.stringify(error) );
		            else console.log(response);
		        });

		        connection.end();

		        return;



		});		

	}

	app.get('/churn', function(req, res) {
		
		var id = req.query.YJ2mRMuIDT; 

		if(id != 'dB0H9DagDh'){
			console.log("Incorrect Churn Credentials");
			res.end();
			return;
		}

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		connection.query("select * from `" + dbconfig.database + "`.unanswered where timeAsked <= DATE_SUB(NOW(), INTERVAL 1 DAY)", function(err, rows, fields) {
			console.log(rows);
			for(var i = 0; i < rows.length; i++){ 

				refund(rows[i].email,rows[i].paymentID,"timeout",rows[i].question);

			}

			connection.end();
			res.end();
		});

	});

	app.get('/clear', function(req, res) {
			
		var id = req.query.YJ2mRMuIDT; 

		if(id != 'dB0H9DagDh'){
			console.log("Incorrect Clear Credentials");
			res.end();
			return;
		}

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		connection.query("delete from `" + dbconfig.database + "`.question where (timeAsked is null or timeAsked = \"\") and (paymentID is null or paymentID = \"\") and initiated <= date_sub(now(), interval 2 hour)", function(err, rows, fields) {
 			connection.end();
		});

		res.end();
	});

	app.get('/go', function(req, res) {
		
		var forms = require('forms');
		var fields = forms.fields;
		var validators = forms.validators;

		var reg_form = forms.create({
		    email: fields.email({ required: true }),
		    question: fields.string({ required: validators.required('You definitely want to ask a question') }),
		    terms:  fields.boolean({
		        required: validators.required('You must accept the terms of use')
		    })
		});

		res.render('go.ejs',{form:reg_form.toHTML()}); // load the index.ejs file
	});

	app.get('/success', function(req, res) {
		res.render('success.ejs'); // load the index.ejs file
	});

	app.get('/login', function(req, res, next){if (!req.isAuthenticated())return next();res.redirect('/profile');},function(req, res) {
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    });

	app.get('/signup', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	app.post('/submit_question', function(req, res) {
		
		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		if(req.body.question == ""){
			console.log("question not filled");
			req.flash("You must ask a question.");
			res.redirect('go');
			return;
		}

		connection.query("insert into `" + dbconfig.database + "`.question (question,email,work) values ('"+req.body.question+"','"+req.body.email+"','"+req.body.work+"')", function(err, rows, fields) {
				
			if(err)throw err;

			connection.query("select LAST_INSERT_ID() as last from `" + dbconfig.database + "`.question", function(err, rows2, fields) {
			 
				var last = rows2[0].last;
				if(err)throw err;

				var returnURL = (process.env.PORT == null)?"http://localhost:8080/process":"http://www.homework.guru/process";
				var cancelURL = (process.env.PORT == null)?"http://localhost:8080/go":"http://www.homework.guru/go";

			    var payment = {
				  "intent": "authorize",
				  "payer": {
				    "payment_method": "paypal" 
				  },
				  "redirect_urls": {
				    "return_url": returnURL+"?id="+last,
				    "cancel_url": cancelURL
				  },
				  "transactions":  [{
				    "amount": {
				      "total": 1.08,
				      "currency": "USD"
				    },
				    "description": "Homework Guru Textbook Solution"
				  }]
				};

				paypal.payment.create(payment, function (error, payment) {
					  if (error) {
					    console.log(error);
					  } else {
					    if(payment.payer.payment_method === 'paypal') {
					      req.session.paymentId = payment.id;
					      var redirectUrl;
					      for(var i=0; i < payment.links.length; i++) {
					        var link = payment.links[i];
					        if (link.method === 'REDIRECT') {
					          redirectUrl = link.href;
					        }
					      }

					      res.redirect(redirectUrl);
					    }
					  }
				});

				connection.end();
			});
		});

		

	});



	app.get('/process', function(req, res) {

		var paymentId = req.session.paymentId;
		var payerId = req.param('PayerID');

		if(!paymentId){res.redirect('/');return;}
		if(!payerId) {res.redirect('/');return;}

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		var execute_payment_details = { "payer_id": payerId };

			paypal.payment.execute(paymentId, execute_payment_details, function(error, payment){
			  if(error){
			    console.error(error);
			  } else {

				var tid = payment.transactions[0].related_resources[0].authorization.id

			  	connection.query("update `" + dbconfig.database + "`.question set timeAsked=now(), paymentID='"+paymentId+"',  authID='"+tid+"', payerID='"+payerId+"' where id="+req.query.id, function(err, rows, fields) {
				
					if(err)throw err;

					connection.query("select * from `" + dbconfig.database + "`.question where id='"+req.query.id+"'", function(err, rows2, fields) {
						
						if(err)throw err;

						sendMail(rows2[0].email,rows2[0].question);
						res.redirect('/success');
						connection.end();

					});
				  
				});
			    
			  }
			});


	   
	});

	app.get('*', function(req, res){
		  res.redirect('/');
	});

};

function sendMail(email, question){

        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'malott.jonathan@gmail.com',
                pass: 'hook9435bench'
            }
        });

        var mandrill = require('node-mandrill')('tDv0RopFBHZb_Br26if3ig');
        mandrill('/messages/send', {
            message: {
                to: [{email: email}],
                from_email: 'no-reply@homework.guru',
                from_name: 'Homework Guru',
                subject: "Homework.Guru Question Received",
                text: "Thank you for your sending in a question to Homework Guru.\n Below is the question you submitted, we'll email you back with a solution within 24 hours or we will refund your money.\n\n"+question
            }
        }, function(error, response){
            if (error) console.log( JSON.stringify(error) );
            else console.log(response);
        });

		


}


function isLoggedIn(req, res, next) {

	if (req.isAuthenticated())
		return next();

	res.redirect('/');
}
