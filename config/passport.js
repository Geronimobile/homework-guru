// config/passport.js

// load all the things we need
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
//var TwitterStrategy  = require('passport-twitter').Strategy;
//var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

// load up the user model
var bcrypt = require('bcrypt-nodejs');
var mysql = require('mysql');
var dbconfig = require('./database');

var configAuth = require('./auth');

// expose this function to our app using module.exports
module.exports = function(passport) {

/*
var connection = mysql.createConnection(dbconfig.connection);

connection.query('USE ' + dbconfig.database,function(err,rows){
if(err)throw err;
//connection.end();
});
*/
    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        var connection = mysql.createConnection(dbconfig.connection);
        connection.query("SELECT * FROM `" + dbconfig.database + "`.users WHERE id = ? ",[id], function(err, rows){
            done(err, rows[0]);
            connection.end();
        });
    });

    passport.use(
        'local-signup',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
           
            if(username == null || username.trim() == ""){
                return done(null, false, req.flash('signupMessage', 'Please enter a username'));
            } else if(password == null || password.trim() == ""){
                return done(null, false, req.flash('signupMessage', 'Please enter a password'));
            } 

            var connection = mysql.createConnection(dbconfig.connection);
            connection.query("SELECT * FROM `" + dbconfig.database + "`.users WHERE username = ?",[username], function(err, rows) {
                if (err){
                    connection.end();
                    return done(err);}
                if (rows.length) {
                    connection.end();
                    console.log("username is taken");
                    return done(null, false, req.flash('signupMessage', 'That email is already in use.'));
                } else {
                    // if there is no user with that username
                    // create the user
                    var newUserMysql = {
                        id:-1,
                        username: username,
                        password: bcrypt.hashSync(password, null, null) // use the generateHash function in our user model
                    };

                    var insertQuery = "INSERT INTO `" + dbconfig.database + "`.users ( username, password, fname, lname ) values (?,?,?,?)";

                    connection.query(insertQuery,[newUserMysql.username, newUserMysql.password, req.body.fname, req.body.lname],function(err, rows2) {
                       connection.query("select LAST_INSERT_ID() as last from `" + dbconfig.database + "`.users", function(err, rows3, fields) {
                       
                            sendWelcome(newUserMysql.username,req.body.lname);

                           newUserMysql.id = rows3[0].last;
                           connection.end();
                           return done(null, newUserMysql);
                          
                      }); 
                   });
                }
            });
        })
    );


function sendWelcome(email, fname){

        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'malott.jonathan@gmail.com',
                pass: 'hook9435bench'
            }
        });

        var url = (process.env.PORT == null)?"http://www.localhost:8080/reset?tkn=":"http://funderful.herokuapp.com/reset?tkn=";

        var mandrill = require('node-mandrill')('tDv0RopFBHZb_Br26if3ig');
        mandrill('/messages/send', {
            message: {
                to: [{email: email}],
                from_email: 'no-reply@crowdnine.com',
                from_name: 'CrowdNine',
                subject: "Crowd9 Password Reset",
                text: '<b>Hello, '+fname+'</br>Thank you for creating an account on Crowd9.co!</b>'
            }
        }, function(error, response){
            if (error) console.log( JSON.stringify(error) );
            else console.log(response);
        });

}

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use(
        'local-login',
        new LocalStrategy({
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) { // callback with email and password from our form
      
            if(username == null || username.trim() == ""){
                return done(null, false, req.flash('loginMessage', 'Please enter a username'));
            } else if(password == null || password.trim() == ""){
                return done(null, false, req.flash('loginMessage', 'Please enter a password'));
            }

            var connection = mysql.createConnection(dbconfig.connection);
            connection.query("SELECT * FROM `" + dbconfig.database + "`.users WHERE username = ?",[username], function(err, rows){
                if (err){
                    connection.end();
                    return done(err);
                }
                if (!rows.length) {
                    connection.end();
                    return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash
                }

                // if the user is found but the password is wrong
                if (!bcrypt.compareSync(password, rows[0].password)){
                    connection.end();
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.')); // create the loginMessage and save it to session as flashdata
                }

                // all is well, return successful user
                connection.end();
                return done(null, rows[0]);
            });
        })
    );
};
