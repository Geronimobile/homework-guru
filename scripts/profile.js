module.exports = function(app){

	var paypal = require('paypal-rest-sdk');	

	paypal.configure({
	  'mode': 'sandbox', //sandbox or live
	  'client_id': 'Ad7vv92aF9xi6rc4rT0uofXBHKc-Io5-6qrDy9U6V6N-AqP2gjDd9LNn_jEgjOt_qjH2anzexm09Zb0f',
	  'client_secret': 'EFqeYCrKB-BdyV2AMFjGSCv1GF0KPtLAqclJcW98Bqo3R0YDtga5kSp5_Fz4PkkfSkI-txNNCscBgbeE'
	});

	app.post('/submit_answer',isLoggedIn, function(req, res) {
		
		if(req.query.id == undefined || req.body.answer == undefined || req.query.email == undefined){
			res.redirect('/profile');
			console.log("stuff is undefined");
			return;
		}

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		connection.query("insert into `" + dbconfig.database + "`.answer (questionID,answer) values ('"+req.query.id+"','"+req.body.answer+"')", function(err, rows, fields) {
			
			if(err)throw err;

			//process payment
			execute(req.query.id,req.query.email,req.body.answer,res);

			connection.end();
		});

	});

	function execute(id,email,answer,res){

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		connection.query("select * from `" + dbconfig.database + "`.question where id = '"+id+"'", function(err, rows, fields) {
			
			if(err)throw err;

			var mysql = require('mysql');
			var dbconfig = require('../config/database');

			payerId = rows[0].payerID;
			authID = rows[0].authID;
			question = rows[0].question;

			var capture_details = {
			  "amount": {
			    "currency": "USD",
			    "total": "1.08" },
			  "is_final_capture": true };

			paypal.authorization.capture(authID, capture_details, function (error, payment) {
			    if (error) {
			    	console.log(error);

					var connection1 = mysql.createConnection(dbconfig.connection);

					connection1.query("update `" + dbconfig.database + "`.question set refunded=1 where id="+id, function(err, rows, fields) {
						
						if(err)throw err;
						res.redirect('/profile');

						sendErrorMail(email);

						connection1.end();

					});

			    } else {

					var tid = payment.id;
			    	
			    	console.log("Payment Successful! ");
					var connection1 = mysql.createConnection(dbconfig.connection);

					connection1.query("update `" + dbconfig.database + "`.question set transactionID='"+payment.id+"' where id="+id, function(err, rows, fields) {
						
						if(err)throw err;
					
						sendAnsweredMail(email,answer,question);
						
						connection1.end();
						res.redirect('/profile');
						return;
					});

			    }

			  
			});

			connection.end();

		});

	}


	app.get('/profile', isLoggedIn, function(req, res) {

		var mysql = require('mysql');
		var dbconfig = require('../config/database');
		var connection = mysql.createConnection(dbconfig.connection);

		connection.query("select count(id) as total from `" + dbconfig.database + "`.question where timeAsked is not null", function(err, rows1, fields) {

			if(err)throw err;

			connection.query("select count(id) as past from `" + dbconfig.database + "`.question where timeAsked is not null and timeAsked > (NOW() - INTERVAL 1 DAY) ", function(err, rows2, fields) {
		
				if(err)throw err;

				connection.query("select count(id) as now from `" + dbconfig.database + "`.unanswered", function(err, rows3, fields) {
			
					if(err)throw err;

					connection.query("select * from `" + dbconfig.database + "`.unanswered order by timeAsked limit 1", function(err, rows4, fields) {
			
						if(err)throw err;

						connection.query("select count(id) as soon from `" + dbconfig.database + "`.unanswered where timeasked > DATE_SUB(NOW(),INTERVAL 24 HOUR) and  timeasked < DATE_SUB(NOW(),INTERVAL 12 HOUR)", function(err, rows5, fields) {
			
							if(err)throw err;

							res.render('profile.ejs', {
								user : req.user,
								total: rows1[0].total,
								past: rows2[0].past,
								now: rows3[0].now,
								question: rows4[0],
								soon:rows5[0].soon
							});

							connection.end();

						});

					});
				});
			});
		});

	});

}

function sendAnsweredMail(email, answer, question){

        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'malott.jonathan@gmail.com',
                pass: 'hook9435bench'
            }
        });

        var mandrill = require('node-mandrill')('tDv0RopFBHZb_Br26if3ig');
        mandrill('/messages/send', {
            message: {
                to: [{email: email}],
                from_email: 'no-reply@homework.guru',
                from_name: 'Homework Guru',
                subject: "Homework.Guru Answer",
                text: "We have an answer for you!\n\n"+answer+ "\n\nThe question you asked:\n\n"+question
            }
        }, function(error, response){
            if (error) console.log( JSON.stringify(error) );
            else console.log(response);
        });

}

function sendErrorMail(email){

        var nodemailer = require('nodemailer');

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'malott.jonathan@gmail.com',
                pass: 'hook9435bench'
            }
        });

        var mandrill = require('node-mandrill')('tDv0RopFBHZb_Br26if3ig');
        mandrill('/messages/send', {
            message: {
                to: [{email: email}],
                from_email: 'no-reply@homework.guru',
                from_name: 'Homework Guru',
                subject: "We couldn't find your answer",
                text: "Unfortunately we couldn't find your answer :( However, we would love to try to find another question for you. We have not charged you anything for this question."
            }
        }, function(error, response){
            if (error) console.log( JSON.stringify(error) );
            else console.log(response);
        });

}

function isLoggedIn(req, res, next) {

	if (req.isAuthenticated())
		return next();

	res.redirect('/');
}

