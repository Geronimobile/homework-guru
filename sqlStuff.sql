#create view unanswered as
select q.* from question q inner join (select id from (select * from question q left outer join answer a on a.questionID = q.id) v where v.answerID is null) v on q.id=v.id

create view unansweredID as
select id from question_answer v where v.answerID is null and v.timeAsked is not null and v.timeAsked <= date_sub(now(), interval 1 hour);


create view unanswered as
select q.* from question q inner join unansweredid u on u.id = q.id where refunded = 0

create view unanswered as
select q.* from question q left outer join answer a on a.questionID = q.id where transactionID is null and timeAsked < date_sub(now(),interval 2 hour ) and timeAsked > date_sub(now(),interval 24 hour )

create view unanswered as
select q.* from question q left outer join answer a on a.questionID = q.id where (transactionID IS NULL OR transactionID ="") and timeAsked < date_sub(now(),interval 2 hour ) and refunded=0 and timeAsked > date_sub(now(),interval 24 hour )

create view answered as
select question.*,answer from answer inner join question on answer.questionID = question.id where refunded = 0

create view stats as
select date(timeAsked) as date, count(id) as count from answered group by date(timeAsked)

delete from question where (timeAsked is null or timeAsked = "") and (paymentID is null or paymentID = "") and initiated <= date_sub(now(), interval 2 hour)

create view unanswered as
select q.*, time_to_sec(timediff(now(), q.timeAsked )) / 3600 as diff from question q left outer join answer a on a.questionID = q.id where (transactionID IS NULL OR transactionID ="") and timeAsked < date_sub(now(),interval 2 hour ) and refunded=0 

select month(time) as month, year(time) as year, count(*) as count from answer group by month(time)

#create view answeredPerMonth as
select month(time) as month, year(time) as year, count(*) as count, count(*) * 1.08 as total, count(*) * .4 as royalties, count(*) * .5 as profit   from answer group by month(time)

select hour(timeAsked), count(*) from question group by hour(timeAsked)